#!/bin/bash

# From https://intoli.com/blog/exit-on-errors-in-bash-scripts/
# exit when any command fails
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

# Arguments
TICKETID=$1
INPUT=$2
REFGENOME=$3
EXTRAARGS=$4

# Containers
TMPCONTAINER=temp
LOGCONTAINER=logs
RESULTCONTAINER=results

# Set azure credentials
source set-credentials.sh

ANALYSISDIR=/datadrive
TMPDIR=$TMPCONTAINER/$TICKETID/logs/
RESULTDIR=$RESULTCONTAINER/$TICKETID/results/
PIPELINEDIR=$ANALYSISDIR/pipeline/$TICKETID/


titan --bams_file $BAMS \
  --config_file $HOME/titan_pypeline/config/${REFGENOME}/azure/titan.yaml \
  --out_dir $RESULTDIR \
  --tmpdir $TMPDIR \
  --pipelinedir $PIPELINEDIR \
  --loglevel DEBUG \
  --sentinal_only $EXTRAARGS \
  --maxjobs 1000 \
  --nocleanup \
  --submit pypeliner.contrib.azure.batchqueue.AzureJobQueue \
  --storage pypeliner.contrib.azure.blobstorage.AzureBlobStorage \
  --submit_config $HOME/titan_pypeline/config/${REFGENOME}/azure/batch.yaml \