from setuptools import setup, find_packages
import versioneer
import os

extra_files = ['scripts/*.py', 'scripts/*.R', 'scripts/readCounter', 'scripts/*.pl']

setup(
    name='titan',
    packages=find_packages(),
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description='Titan pipeline',
    author='',
    author_email='',
    entry_points={'console_scripts': ['titan = titan.run:main']},
    package_data={'': extra_files},
)
