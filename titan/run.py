import os
import argparse
import utils
import pypeliner
import pypeliner.managed as mgd
from workflows import titan


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    pypeliner.app.add_arguments(parser)

    parser.add_argument('--bams_file',
                        required=True,
                        help='''Path to input bam CSV.''')
    parser.add_argument('--config_file',
                        required=True,
                        help='''Path to yaml config file.''')
    parser.add_argument('--out_dir',
                        required=True,
                        help='''Path to output files.''')

    args = vars(parser.parse_args())

    return args


def main():
    args = parse_args()
    config = utils.load_config(args)

    pyp = pypeliner.app.Pypeline(config=args)

    tumour_bam, normal_bam, sample_ids = utils.read_bams_file(args['bams_file'])

    workflow = pypeliner.workflow.Workflow()

    workflow.setobj(
        obj=mgd.OutputChunks('sample_id'),
        value=tumour_bam.keys(),
    )

    tumour_bai = dict([(sample_id, str(tumour_bam[sample_id]) + '.bai')
                         for sample_id in sample_ids])
    normal_bai = dict([(sample_id, str(normal_bam[sample_id]) + '.bai')
                         for sample_id in sample_ids])

    output_dir = os.path.join(args['out_dir'], '{sample_id}')

    titan_outfile_txt = os.path.join(output_dir, '{sample_id}_titan_outfile.txt')
    titan_outfile_params_txt = os.path.join(output_dir, '{sample_id}_titan_outfile_params.txt')
    titan_plot_dir = os.path.join(output_dir, '{sample_id}_titan_plots/')
    outigv_seg = os.path.join(output_dir, 'outigv.seg')
    pygene_segs_txt = os.path.join(output_dir, '{sample_id}_titan_pygenes_segs.txt')

    workflow.subworkflow(
        name='titan',
        func=titan.create_titan_workflow,
        axes=('sample_id',),
        args=(
            mgd.InputFile('normal_bam', 'sample_id', fnames=normal_bam),
            mgd.InputFile('normal_bai', 'sample_id', fnames=normal_bai),
            mgd.InputFile('tumour_bam', 'sample_id', fnames=tumour_bam),
            mgd.InputFile('tumour_bai', 'sample_id', fnames=tumour_bai),
            mgd.OutputFile(titan_outfile_txt, 'sample_id'),
            mgd.OutputFile(titan_outfile_params_txt, 'sample_id'),
            mgd.OutputFile(outigv_seg, 'sample_id'),
            mgd.OutputFile(pygene_segs_txt, 'sample_id'),
            config,
        ),
    )

    pyp.run(workflow)

if __name__ == '__main__':
    main()
