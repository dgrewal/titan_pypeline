import os
import pypeliner
import multiprocessing
import subprocess
from pypeliner_utils import vcfutils


scripts_directory = os.path.join(os.path.realpath(os.path.dirname(__file__)), 'scripts')


def _run_museq_worker(tumour_bam, normal_bam, out, log, config, chrom):
    '''
    Run museq script for each chromosome

    :param tumour_bam: path to tumour bam
    :param normal_bam: path to normal bam
    :param out: temporary output vcf file for the chromosome 
    :param log: temporary log file (museq.log)
    :param config: dictionary of parameters for the run
    :param chrom: chromosome number
    '''

    script = os.path.join(config['museq_params']['mutationseq'], 'preprocess.py')
    conf = os.path.join(config['museq_params']['mutationseq'], 'metadata.config')
    model = config['museq_params']['mutationseq_model']
    reference = config['reference_genome']

    cmd = ['python', script, 'normal:' + normal_bam, 'tumour:' + tumour_bam,
           'reference:' + reference, 'model:' + model, '--out', out,
           '--log', log, '--config', conf, '--interval', chrom, '--verbose']

    # Using pypeline.commandline.execute(*cmd) freezes the program
    subprocess.call(cmd)


def run_museq(tumour_bam, tumour_bai, normal_bam, normal_bai, out, log, config):
    '''
    Run museq script for all chromosomes and merge vcf files

    :param tumour_bam: path to tumour bam
    :param tumour_bai: path to tumour bai
    :param normal_bam: path to normal bam
    :param normal_bai: path to normal bai
    :param out: temporary output vcf file for the merged vcf files (museq.vcf)
    :param log: temporary log file (museq.log)
    :param config: dictionary of parameters for the run
    '''

    count = config.get('threads', multiprocessing.cpu_count())
    pool = multiprocessing.Pool(processes=count)

    output_vcf = {}
    tasks = []

    chroms = config['chromosomes']

    for chrom in chroms:

        outfile = os.path.join(os.path.dirname(out), 'chr' + str(chrom) + '.vcf.tmp')

        task = pool.apply_async(
            _run_museq_worker,
            args=(
                tumour_bam,
                normal_bam,
                outfile,
                log,
                config,
                chrom,
            )
        )

        tasks.append(task)
        output_vcf[chrom] = outfile

    pool.close()
    pool.join()

    [task.get() for task in tasks]

    # For some reason, mutation seq does not run for all chromosomes with apply_async
    for chrom, vcf in output_vcf.iteritems():
        if not os.path.exists(vcf):
            _run_museq_worker(tumour_bam, normal_bam, vcf, log, config, chrom)

    vcfutils.concatenate_vcf(output_vcf, out)


def convert_museq_vcf2counts(infile, outfile, config):
    '''
    Transform museq vcf file to a text file of counts

    :param infile: merged temporary vcf file from museq run (museq.vcf)
    :param outfile: temporary text file of counts (museq_postprocess.txt)
    :param config: dictionary of parameters for the run
    '''

    script = os.path.join(scripts_directory, 'transform_vcf_to_counts.py')
    positions_file = config['convert_museq_vcf2counts_params']['positions_file']

    cmd = ['python', script, '--infile', infile, '--outfile', outfile,
            '--positions_file', positions_file]

    pypeliner.commandline.execute(*cmd)


def run_readcounter(bam, bai, outfile, config):
    '''
    Run readCounter on the input bam file

    :param bam: path to normal or tumour bam
    :param bai: path to normal or tumour bai
    :param outfile: temporary wig file
    :param config: dictionary of parameters for the run
    '''

    readcounter = os.path.join(scripts_directory, 'readCounter')
    w = config['readcounter_params']['w']
    q = config['readcounter_params']['q']
    chrs = ','.join(config['chromosomes'])

    cmd = [readcounter, '-w', w, '-q', q, '-c', chrs, bam, '>', outfile]

    pypeliner.commandline.execute(*cmd)


def calc_correctreads_wig(tumour_wig, normal_wig, outfile, config):
    '''
    Run script to calculate correct reads

    :param tumour_wig: temporary wig file for tumour from readCounter
    :param normal_wig: temporary wig file for normal from readCounter
    :param outfile: temporary output text file (correct_reads.txt)
    :param config: dictionary of parameters for the run
    '''

    script = os.path.join(scripts_directory, 'correctReads.R')
    gc = config['calc_correctreads_wig_params']['gc']
    map_wig = config['map']
    target_list = 'NULL'
    genome_type = config['genome_type']

    cmd = ['Rscript', script, tumour_wig, normal_wig, gc, map_wig, 
            target_list, outfile, genome_type]

    pypeliner.commandline.execute(*cmd)


def run_titan(infile, cnfile, outfile, obj_outfile, outparam, config):
    '''
    Run titan script

    :param infile: temporary text file of counts (museq_postprocess.txt)
    :param cnfile: temporary text file of correct reads (correct_reads.txt)
    :param outfile: output text file ({sample_id}_titan_outfile.txt)
    :param obj_outfile: temporary output R object for plotting (titan_outfile_obj.RData)
    :param outparam: output text file of parameters ({sample_id}_titan_outfile_params.txt)
    :param config: dictionary of parameters for the run
    '''

    script = os.path.join(scripts_directory, 'titan.R')
    map_wig = config['map']
    num_clusters = config['titan_params']['num_clusters']
    ploidy = config['titan_params']['ploidy']
    genome_type = config['genome_type']
    y_threshold = config['titan_params']['y_threshold']

    num_cores = 4
    myskew = 0
    estimate_ploidy = 'TRUE'
    normal_param_nzero = 0.5
    normal_estimate_method = 'map'
    max_iters = 50
    pseudo_counts = 1e-300
    txn_exp_len = 1e16
    txn_z_strength = 1e6
    alpha_k = 15000
    alpha_high = 20000
    max_copynumber = 8
    symmetric = 'TRUE'
    chrom = 'NULL'
    max_depth = 1000

    sample_id = os.path.basename(outfile).split('_')[0]

    cmd = ['Rscript', script, sample_id, infile, cnfile, map_wig, num_clusters,
            num_cores, ploidy, outfile, outparam, myskew, estimate_ploidy, 
            normal_param_nzero, normal_estimate_method, max_iters, pseudo_counts,
            txn_exp_len, txn_z_strength, alpha_k, alpha_high, max_copynumber,
            symmetric, obj_outfile, genome_type, chrom, y_threshold, max_depth]

    pypeliner.commandline.execute(*cmd)


def plot_titan(obj_file, titan_input, config):
    '''
    Plot titan results

    :param obj_file: temporary R object that was output from the titan run (titan_outfile_obj.RData)
    :param titan_input: parameter file from titan run
    :param config: dictionary of parameters for the run
    '''

    script = os.path.join(scripts_directory, 'plot_titan.R')
    num_clusters = config['titan_params']['num_clusters']
    ploidy = config['titan_params']['ploidy']

    output_dir = os.path.dirname(titan_input)
    chrom = 'c(1:22,\'X\')'

    cmd = ['Rscript', script, obj_file, output_dir, num_clusters, chrom, ploidy]

    pypeliner.commandline.execute(*cmd)


def calc_cnsegments_titan(infile, outigv, outfile):
    '''
    Generate segment files from the titan output (IGV compatible)

    :param infile: text file output from titan run
    :param outigv: output seg file (outigv.seg)
    :param outfile: temporary text output file (segs.txt)
    '''

    script = os.path.join(scripts_directory, 'createTITANsegmentfiles.pl')

    sample_id = os.path.basename(infile).split('_')[0]
    symmetric = '1'

    cmd = ['perl', script, '-id=' + sample_id, '-infile=' + infile,
            '-outfile=' + outfile, '-outIGV=' + outigv, 
            '-symmetric=' + symmetric]

    pypeliner.commandline.execute(*cmd)


def annot_pygenes(infile, outfile, config):
    '''
    Pygene annotation for titan

    :param infile: temporary text input file (segs.txt)
    :param outfile: output text file ({sample_id}_titan_pygenes_segs.txt)
    :param config: dictionary of parameters for the run
    '''

    script = os.path.join(scripts_directory, 'pygene_annotation.py')
    gene_sets_gtf = config['pygene_params']['gene_sets_gtf']

    cmd = ['python', script, '--infile', infile, '--outfile', outfile,
            '--gene_sets_gtf', gene_sets_gtf]

    pypeliner.commandline.execute(*cmd)

