import os
import pypeliner
import pypeliner.managed as mgd
import tasks

def create_titan_workflow(normal_bam, normal_bai, tumour_bam, tumour_bai, titan_outfile_txt, titan_outfile_params_txt,
                            outigv_seg, pygene_segs_txt, config):

    workflow = pypeliner.workflow.Workflow()

    workflow.transform(
        name='run_museq',
        ctx={'mem': config['memory']['high'], 'pool_id': config['pools']['multicore'], 'ncpus': config['threads'], 'walltime':'06:00'},
        func=tasks.run_museq,
        args=(
            mgd.InputFile(tumour_bam),
            mgd.InputFile(tumour_bai),
            mgd.InputFile(normal_bam),
            mgd.InputFile(normal_bai),
            mgd.TempOutputFile('museq.vcf'),
            mgd.TempOutputFile('museq.log'),
            config,
        ),
    )

    workflow.transform(
        name='convert_museq_vcf2counts',
        ctx={'mem': config['memory']['high'], 'pool_id': config['pools']['highmem'], 'ncpus': 1, 'walltime':'01:00'},
        func=tasks.convert_museq_vcf2counts,
        args=(
            mgd.TempInputFile('museq.vcf'),
            mgd.TempOutputFile('museq_postprocess.txt'),
            config,
        ),
    )

    workflow.transform(
        name='run_readcounter_tumour',
        ctx={'mem': config['memory']['high'], 'pool_id': config['pools']['highmem'], 'ncpus': 1, 'walltime':'01:00'},
        func=tasks.run_readcounter,
        args=(
            mgd.InputFile(tumour_bam),
            mgd.InputFile(tumour_bai),
            mgd.TempOutputFile('tumour.wig'),
            config,
        ),
    )

    workflow.transform(
        name='run_readcounter_normal',
        ctx={'mem': config['memory']['high'], 'pool_id': config['pools']['highmem'], 'ncpus': 1, 'walltime':'01:00'},
        func=tasks.run_readcounter,
        args=(
            mgd.InputFile(normal_bam),
            mgd.InputFile(normal_bai),
            mgd.TempOutputFile('normal.wig'),
            config,
        ),
    )

    workflow.transform(
        name='calc_correctreads_wig',
        ctx={'mem': config['memory']['low'], 'pool_id': config['pools']['standard'], 'ncpus': 1, 'walltime':'01:00'},
        func=tasks.calc_correctreads_wig,
        args=(
            mgd.TempInputFile('tumour.wig'),
            mgd.TempInputFile('normal.wig'),
            mgd.TempOutputFile('correct_reads.txt'),
            config,
        ),
    )

    workflow.transform(
        name='run_titan',
        ctx={'mem': config['memory']['high'], 'pool_id': config['pools']['highmem'], 'ncpus': 1, 'walltime':'06:00'},
        func=tasks.run_titan,
        args=(
            mgd.TempInputFile('museq_postprocess.txt'),
            mgd.TempInputFile('correct_reads.txt'),
            mgd.OutputFile(titan_outfile_txt),
            mgd.TempOutputFile('titan_outfile_obj.RData'),
            mgd.OutputFile(titan_outfile_params_txt),
            config,
        )
    )

    workflow.transform(
        name='plot_titan',
        ctx={'mem': config['memory']['low'], 'pool_id': config['pools']['standard'], 'ncpus': 1, 'walltime':'01:00'},
        func=tasks.plot_titan,
        args=(
            mgd.TempInputFile('titan_outfile_obj.RData'),
            mgd.InputFile(titan_outfile_params_txt),
            config,
        ),
    )

    workflow.transform(
        name='calc_cnsegments_titan',
        ctx={'mem': config['memory']['low'], 'pool_id': config['pools']['standard'], 'ncpus': 1, 'walltime':'01:00'},
        func=tasks.calc_cnsegments_titan,
        args=(
            mgd.InputFile(titan_outfile_txt),
            mgd.OutputFile(outigv_seg),
            mgd.TempOutputFile('segs.txt'),
        ),
    )

    workflow.transform(
        name='annot_pygenes',
        ctx={'mem': config['memory']['low'], 'pool_id': config['pools']['standard'], 'ncpus': 1, 'walltime':'01:00'},
        func=tasks.annot_pygenes,
        args=(
            mgd.TempInputFile('segs.txt'),
            mgd.OutputFile(pygene_segs_txt),
            config,
        ),
    )

    return workflow
